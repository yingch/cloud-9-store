package sheridan;

import java.util.regex.Pattern;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName) {
		Pattern special = Pattern.compile ("[!@#$%&*()_+=|<>?{}\\[\\]~-]");
		boolean hasSpecial = special.matcher(loginName).find();
		
		if (hasSpecial) {
			return false;
		}
			
		
		if (loginName.length() < 6) {
			return false;
		}
		
		if (Character.isDigit(loginName.charAt(0))) {
			return false;
		}
		return true;
	}
	
}
