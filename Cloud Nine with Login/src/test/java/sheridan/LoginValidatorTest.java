package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "yingchen" ) );
	}
	
	@Test
	public void testIsValidLoginException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Ying___Chen" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryIn( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "ying_chen123+" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "1yingchen" ) );
	}
	
	
	
	@Test
	public void testHasValidLengthRegular( ) {
		assertTrue("Invalid length" , LoginValidator.isValidLoginName( "yingchen" ) );
	}
	
	@Test
	public void testHasValidLengthException( ) {
		assertFalse("Invalid length" , LoginValidator.isValidLoginName( "" ) );
	}
	
	@Test
	public void testHasValidLengthBoundaryIn( ) {
		assertTrue("Invalid length" , LoginValidator.isValidLoginName( "yingch" ) );
	}
	
	@Test
	public void testHasValidLengthBoundaryOut( ) {
		assertFalse("Invalid length" , LoginValidator.isValidLoginName( "yingc" ) );
	}

}
